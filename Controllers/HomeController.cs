using Microsoft.AspNetCore.Mvc;

namespace ImageOptimizer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}
