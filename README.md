# Image Optimizer

Open Source Image compressor service based on Magick.NET

[Try it!](http://image-optimizer.rodrigovargas.com.br/)

## Using the API

POST /api/shrink HTTP/1.1

`
{
    output: 'http://path-to-your-shrinked-image
}
`

## Author
#### Rodrigo Vargas

- Checkout my <a href="https://rodrigovargas.com.br" title="Full-Stack Web Developer" target="_blank">Web Developer Portfolio Website</a>
