import React, { useCallback, useState } from 'react';
import { useDropzone } from 'react-dropzone';

const App = () => {
   const [state, setState] = useState([]);

   const onDrop = useCallback((acceptedFiles) => {
      const data = new FormData();

      data.append('file', acceptedFiles[0]);

      fetch('https://localhost:5001/api/home', {
         method: 'POST',
         body: data
      })
      .then(response => response.json())
      .then(response => {
         const newState = [...state];

         newState.push({
            shrinkedImage: response.output
         });

         setState(newState);
      })
   }, []);

   const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });

   return (
      <div>
         <div {...getRootProps()}>
            <input {...getInputProps()} />
            {isDragActive ? (
               <p>Drop the files here ...</p>
            ) : (
               <p>Drag 'n' drop some files here, or click to select files</p>
            )}
         </div>

         <div className="photos">
            <h2>Photos</h2>
            {
               state && state.map((photo, index) => {
                  return(
                     <div key={index}>
                        <h2>Fotin!</h2>
                        <img src={photo.shrinkedImage} alt="" />
                     </div>
                  )
               })
            }
         </div>
      </div>
   );
};

export default App;
