const path = require('path');

module.exports = {
   mode: 'development',
   entry: {
      main: './src/index.tsx',
   },
   output: {
      path: path.resolve(__dirname, '../wwwroot'),
      filename: '[name].js',
   },
   module: {
      rules: [
         {
            test: /\.(tsx)$/,
            exclude: /node_modules/,
            use: ['babel-loader'],
         },
      ],
   },
};
