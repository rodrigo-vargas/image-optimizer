using Newtonsoft.Json;

namespace ImageOptimizer.Areas.API.Models.Output
{
    public class OutputRequest
    {
        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("operation")]
        public string Operation { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }
    }
}