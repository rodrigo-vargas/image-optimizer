using System.IO;
using ImageOptimizer.Areas.API.Models.Output;
using ImageOptimizer.BusinessLogic;
using Microsoft.AspNetCore.Mvc;

namespace ImageOptimizer.Areas.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OutputController : ControllerBase
    {
        [HttpGet("{imageHash}")]
        public ActionResult Index(
            string imageHash,
            [FromServices] IStorageLogic logic
        )
        {
            Stream fileStream = logic.GetStreamOfHash(imageHash);

            if (fileStream == null)
                return NotFound();

            return File(fileStream, "image/jpeg");
        }

        [HttpPost("{imageHash}")]
        public ActionResult Post(
            string imageHash,
            [FromBody] OutputRequest request,
            [FromServices] IImageLogic logic,
            [FromServices] IStorageLogic storageLogic
        )
        {
            Stream fileStream = storageLogic.GetStreamOfHash(imageHash);

            var result = logic.Manipulate(fileStream, request);

            return File(result, "image/jpeg");
        }
    }
}