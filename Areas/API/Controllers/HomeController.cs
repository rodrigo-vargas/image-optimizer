﻿using System;
using System.IO;
using System.Threading.Tasks;
using ImageMagick;
using ImageOptimizer.BusinessLogic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ImageOptimizer.Areas.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public string Index()
        {
            return "1.0.0";
        }

        [HttpPost]
        public async Task<IActionResult> Shrink(
            IFormFile file,
            [FromServices] IImageLogic logic
        )
        {
            if (file == null || file.Length == 0)
                return BadRequest();

            Guid generatedGuid = Guid.NewGuid();

            var fileName = string.Format("{0}{1}", generatedGuid, Path.GetExtension(file.FileName));

            await logic.CompressImage(file, fileName, 40);

            var generatedEndpoint = string.Format("{0}://{1}/{2}/{3}", Request.Scheme, Request.Host, "api/output", fileName);

            return Ok(new {
                output = generatedEndpoint
            });
        }
    }
}
