using System.IO;
using System.Threading.Tasks;
using ImageMagick;
using ImageOptimizer.Areas.API.Models.Output;
using Microsoft.AspNetCore.Http;

namespace ImageOptimizer.BusinessLogic
{
    public interface IImageLogic
    {
        Task CompressImage(IFormFile file, string fileName, int quality);
        Stream Manipulate(Stream fileStream, OutputRequest request);
    }

    public class ImageLogic : IImageLogic
    {
        private readonly IStorageLogic _storageLogic;

        public ImageLogic(IStorageLogic storageLogic)
        {
            _storageLogic = storageLogic;
        }

        public async Task CompressImage(
            IFormFile file,
            string fileName,
            int quality
        )
        {
            using (var stream = new MemoryStream())
            {
                await file.CopyToAsync(stream);
                stream.Seek(0, SeekOrigin.Begin);

                var image = new MagickImage(stream);
                image.Strip();
                image.Interlace = Interlace.Plane;
                image.GaussianBlur(0.05, 0.05);
                image.Quality = quality;

                image.Write(stream);

                stream.Seek(0, SeekOrigin.Begin);

                var optimizer = new ImageMagick.ImageOptimizer()
                {
                    OptimalCompression = false
                };
                optimizer.Compress(stream);

                await _storageLogic.SaveFile(stream, fileName);
            }
        }

        public Stream Manipulate(Stream fileStream, OutputRequest request)
        {
            var compressedImageStream = new MemoryStream();
            compressedImageStream.Seek(0, SeekOrigin.Begin);

            var image = new MagickImage(fileStream);

            var size = new MagickGeometry(request.Width, request.Height);
            
            size.IgnoreAspectRatio = false;

            image.Resize(size);

            image.Write(compressedImageStream);

            compressedImageStream.Seek(0, SeekOrigin.Begin);

            return compressedImageStream;
        }
    }
}
