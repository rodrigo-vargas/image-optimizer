using System.IO;
using System.Threading.Tasks;

namespace ImageOptimizer.BusinessLogic
{
    public interface IStorageLogic
    {
        Task SaveFile(Stream stream, string fileName);
        FileStream GetStreamOfHash(string imageHash);
    }

    public class StorageLogic : IStorageLogic
    {
        private const string UPLOADS_FOLDER = "uploads";

        public FileStream GetStreamOfHash(string imageHash)
        {
            string path = Path.Combine(UPLOADS_FOLDER, imageHash);

            return new FileStream(path, FileMode.Open);
        }

        public async Task SaveFile(Stream stream, string fileName)
        {
            string path = Path.Combine(UPLOADS_FOLDER, fileName);

            using Stream fileStream = new FileStream(path, FileMode.Create);
            await stream.CopyToAsync(fileStream);
        }
    }
}